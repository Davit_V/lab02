/**
 * Lab 02
 * Main Method
 * 
 * Davit Voskerchyan
 * Student Id: 2034100
 */
public class BikeStore{
    public static void main(String[] args) {
        Bicycle[] bicycle = new Bicycle[4];

        //Intiliazing the 4 bicycle objects
        bicycle[0] = new Bicycle("Honda", 12, 60);
        bicycle[1] = new Bicycle("Toyota", 40, 620);
        bicycle[2] = new Bicycle("Rolls Royce", 9, 20);
        bicycle[3] = new Bicycle("Air Canada", 4, 1000);

       /* 
        * Normal for loop 
            for( int i=0; i<bicycle.length;i++){
               System.out.println("Bike " + (i+1) + ".\n" + bicycle[i] + "\n");
        } */
     
      // For each loop
        for (Bicycle bicycle2 : bicycle) {
            System.out.println(bicycle2);
        }
    }
}
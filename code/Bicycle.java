/**
 * Lab 2
 * 
 * Object
 * 
 * Davit Voskerchyan
 * Student Id: 2034100
 */
public class Bicycle{
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;

    //Constructor
    public Bicycle(String newManufacturer, int newNumberGears, double newMaxSpeed){
        //Initializing variables
        this.manufacturer = newManufacturer;
        this.numberGears = newNumberGears;
        this.maxSpeed = newMaxSpeed;
    }

    //Overiding toString()
    public String toString(){
        return "The Manufacturer: " + this.manufacturer + "\n" + 
                "The numberGears: " + this.numberGears + "\n" +
                "The maxSpeed: " + this.maxSpeed;
    }
    // Getter methods
    public String getManufacturer(){
        return this.manufacturer;
    }
    public int getNumberGears(){
        return this.numberGears;
    }
    public double getMaxSpeed(){
        return this.maxSpeed;
    }

}